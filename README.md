# Technologies

* React
* Redux
* Typescript
* SASS
* Firebase


# Getting Started

The project is connected to BD Provisional in Firebase, the credentials are in field: `./src/utils/database/config.ts`, (just is provisional). for start the project, just run `npm i` for install dependences and after run `npm start`, Already! the project is running.

The problem, i solved with 3 steps:

- One Step: Form for create the Pizza
- Second Step: Form for assign Pizza created
-  Third Step: Table with information about pizza's
