import { useEffect, useState } from 'react';
import './App.scss';
import Modal from './components/Modal';
import PizzaData from './components/PizzaData';
import { IngredientsArrayType, PizzaInfoType, StateAppType } from './types';
import ingredients from './utils/ingredients.json';
import headersTable from './utils/headers.json';
import { connect } from 'react-redux';
import * as mapActions from './store/modules/products/actions';
import { currency, labelsIngredients } from './utils/pipes';
import AssignUser from './components/AssignUser';


type TypeProps = {
  products: PizzaInfoType[],
  getProducts: () => void,
  setLocalProduct: (product:PizzaInfoType) => void
}

const App:React.FC<TypeProps> = ({ products, getProducts, setLocalProduct }) => {

  const [modalPizza, setModalPizza] = useState(false);
  const [modalAssign, setModalAssign] = useState(false);
  const [modePizzaData, setModePizzaData] = useState<'create' | 'read'>('create');
  const [productSelect, setProductSelect] = useState({} as PizzaInfoType);
  const [itemsDrop, setItemsDrop] = useState<IngredientsArrayType[]>([]);

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    console.log('get all products', products);
  }, [products]);


  useEffect(() => {
    if(!modalPizza) {
      // * When close modal, refresh items drop
      setItemsDrop(ingredients.map(item => ({
        ...item,
        isSelect: false
      })));
      // * when close modal assign clear data product select
      setProductSelect({} as PizzaInfoType);
      // * and refresh mode
      setModePizzaData('create');
    }
  }, [modalPizza]);

  useEffect(() => {
    // * when close modal assign clear data product select
    if(!modalAssign)
      setProductSelect({} as PizzaInfoType);
  }, [modalAssign]);



  const toggleModalPizza = () => {
    setModalPizza(prev => !prev);
  }

  const toggleModalAssign = () => {
    setModalAssign(prev => !prev);
  }

  const openModalAssign = (
    product:PizzaInfoType,
    e:React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();

    setProductSelect(product);
    toggleModalAssign();
  };


  // * GET PRODUCTS FOR TABLE
  const fetchProducts = async () => {
    await getProducts();
  }

  //
  const openInfoProduct = (
    product: PizzaInfoType,
    e:React.MouseEvent<HTMLTableRowElement, MouseEvent>
  ) => {
    e.stopPropagation();
    setModePizzaData('read');
    setLocalProduct(product);
    toggleModalPizza();
  }


  return (
    <div className="App">
      <header>
        <img src={require('./assets/habi_logo.jpeg')} alt="Logo habi" />
        <section>
          <div>
              <p>HB</p>
          </div>
          <h4>Habi User</h4>
        </section>
      </header>


      <main>
        {/* Top inputs main */}
        <section className='d-flex justify-between align-center w-100'>
            <input type="text" name="search" placeholder='Buscar' className='w-30' />
            {!modalPizza && (
              <button className='purple w-20' onClick={toggleModalPizza}>Nueva Pizza</button>
            )}
        </section>

        {/* Table */}

        <table>
          <thead>
            <tr>
              {headersTable.map(header => (
                <th key={header.id}>{header.label}</th>
              ))}
              <th>Asignado</th>
            </tr>
          </thead>

          {products.length > 0 && (
            <tbody>
              {products.map((product, key) => (
                <tr key={key} onClick={(e) => openInfoProduct(product, e)} className='pointer'>
                  <td>{product.id}</td>
                  <td>{product.createAt.toDate().toDateString()}</td>
                  <td>{product.name}</td>
                  <td>{labelsIngredients(product.ingredients, true)}</td>
                  <td>{currency(product.price)}</td>
                  <td>
                    {
                      product.assign && product.assign.name || (
                        <button className='lightBlue' onClick={(e) => openModalAssign(product, e)}>
                          Asignar
                        </button>
                      )
                    }
                  </td>
                </tr>
              ))}
            </tbody>
          )}

        </table>

      </main>


      <Modal active={modalPizza} closeModal={toggleModalPizza}>
        <PizzaData mode={modePizzaData} ingredients={itemsDrop} closeModal={toggleModalPizza} />
      </Modal>

      {/* Assign user modal! */}
      <Modal active={modalAssign} closeModal={toggleModalAssign}>
          <AssignUser product={productSelect} closeModal={toggleModalAssign} />
      </Modal>
    </div>
  );
}

const mapStateToProps = (state:StateAppType) => ({
  products: state.productsReducer.products
});

export default connect(mapStateToProps, mapActions)(App);
