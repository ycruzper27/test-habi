import { useEffect, useState } from 'react';
import { ModalType } from '../types';


const Modal:React.FC<ModalType> = ({ children, active, closeModal }) => {

  const [classStateModal, setClassStateModal] = useState('')

  useEffect(() => {
    if(active)
      setClassStateModal('active');
    else setClassStateModal('');
  }, [active]);


  return (
    <>
      {active && (
        <div className={`modal ${classStateModal}`}>
            <a className="close" onClick={() => closeModal()}>
              <img src={require('../assets/close.png')} alt="close icon" />
            </a>
            <div className="modal-content">
              {children}
            </div>
        </div>
      )}
    </>
  )
}

export default Modal