import Dropdown from "./Dropdown"
import { PizzaInfoType, StateAppType, TypePizzaComponent } from '../types';
import { useEffect, useState } from "react";
import { calcTotal, currency, labelsIngredients } from "../utils/pipes";
import * as mapActions from '../store/modules/products/actions';
import { connect } from "react-redux";
import Loader from "./Loader";


const PizzaData:React.FC<TypePizzaComponent> = ({
    mode, productSelected, ingredients,
    getProduct, addProduct, closeModal,
    getProducts
  }) => {

  const DOUGH = {
    name: 'dough',
    label: 'Masa',
    price: 10000,
    id: 12345
  };

  const [product, setProduct] = useState({} as PizzaInfoType);
  const [fieldsIncomplete, setFieldsIncomplete] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // * Add info if exist
  useEffect(() => {
    if(productSelected && mode == 'read')
      console.log('Change product Redux', productSelected);
      setProduct(productSelected);
  }, [productSelected]);

  useEffect(() => {
    if(product.ingredients && product.ingredients.length > 0){
      // * each time that product update, calc again the price
      const price = calcTotal([...product.ingredients, DOUGH]);
      setInfoProduct(price, 'price');
    } else {
      setInfoProduct(10000, 'price');
    };

  }, [product.ingredients])

  const setInfoProduct = (payload: string | number | any[], key: string) => {
    setFieldsIncomplete(false);
    setProduct({
      ...product,
      [key]: payload
    });
  };

  const savePizza = async () => {
    setIsLoading(true);
      if(!product.name || (!product.ingredients || product.ingredients.length < 15))
        setFieldsIncomplete(true);
      else {
        await addProduct({...product, dough: 10000});
        // * After close modal and getAllProducts
        closeModal();
        await getProducts();
      }
    setIsLoading(false);
  }

  return (
    <div className="d-flex w-100 justify-center flex-column align-center pizzaContainer">
      <Loader active={isLoading} />

      <h1 className="text-purple text-center mb-2 mt-2">
        {
          mode === 'read'
          ? `Pizza ${productSelected?.name || ''}`
          : 'Crear pizza'
        }
      </h1>

      <img src={require('../assets/pizza.jpeg')} alt="pizza image" className="w-70 mb-1" />

      { fieldsIncomplete && (
        <p className="text-lightRed weight-bold mb-1 mt-1 font-sm">
          Hay campos que faltan por ingresar información
        </p>
      )}

        {/* JUST FOR CREATE MODE */}

      { mode === 'create' ? (
        <>
          <input type="text" name="name" onChange={(e) => setInfoProduct(e.target.value, 'name')}
            placeholder="Nombre de la pizza" className="w-70 mt-1" />

            {!product.ingredients || product.ingredients.length < 15 ? (
              <p className="font-xs weight-bold text-gray text-center mt-2 mb-1">
                ** Recuerda que debes ecoger minimo 15 ingredientes **
              </p>
            ): null}

          <Dropdown onChange={(ev:any[]) => setInfoProduct(ev, 'ingredients')}
              items={ingredients} labelBtn="Ingredientes" classBtn="w-70" />

          { product.price ?  (
            <p className="text-gray weight-bold mt-2 mb-1 font-md">
              Ingredientes + Masa = {currency(product.price) }
            </p>
          ): null}

          <button className="w-70 purple mt-1" onClick={savePizza}>Guardar</button>

        </>
      ): (
        productSelected && (
          <>
            <p className="text-gray mt-1 mb-1 font-sm">
              <span className="weight-bold">Id: </span>
              {productSelected.id}
            </p>

            {productSelected.assign && (
              <p className="text-gray mt-1 mb-1 font-sm">
                <span className="weight-bold">Usuario Asignado: </span>
                {productSelected.assign.name}
              </p>
            )}

            <p className="text-gray mt-1 mb-1 font-sm">
              <span className="weight-bold">Fecha: </span>
              {productSelected.createAt.toDate().toDateString()}
            </p>
            <p className="text-gray mt-1 mb-1 font-sm">
              <span className="weight-bold">Nombre: </span>
              {productSelected.name.toUpperCase()}
            </p>
            <p className="text-gray mt-1 mb-1 font-sm">
              <span className="weight-bold">Precio: </span>
              {currency(productSelected.price)}
            </p>
            <p className="text-gray mt-1 mb-1 font-sm">
              <span className="weight-bold">Ingedientes: </span>
              {labelsIngredients(productSelected.ingredients, true, true)}
            </p>
          </>
        )
      )}

    </div>
  )
}


const mapStateToProps = (state:StateAppType) => {
  return {
    productSelected: state.productsReducer.product,
  };
};

export default connect(mapStateToProps, mapActions)(PizzaData);