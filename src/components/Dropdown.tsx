import { useEffect, useState } from 'react'
import { IngredientsType, IngredientBasicInfo, TypeDropdown } from '../types'
import { currency, labelsIngredients } from '../utils/pipes';


const Dropdown:React.FC<TypeDropdown> = ({ classBtn, items, labelBtn, disabled, onChange }) => {

    const [menuState, setMenuState] = useState(false);
    const [list, setList] = useState<IngredientsType[]>([]);
    const [labelItemsSelected , setLabelItemsSelected] = useState('')

    useEffect(() => {
        setList(items);
    }, []);

    useEffect(() => {
        watchListLabelSelected();
    }, [list]);

    const watchListLabelSelected = () => {
        let labels = labelsIngredients(list);
        setLabelItemsSelected(labels);
    }

    const toggleMenu = () => {
        setMenuState(prev => !prev);
    };

    const toggleItemSelect = (item:IngredientsType) => {
        const listUpdated = list.map(listItem => {
            if(listItem.id === item.id){
                listItem.isSelect = !listItem.isSelect;
            }

            return listItem;
        });
        // * Send data to method callback
        if(onChange) {
            const products = listUpdated.reduce((acc:IngredientBasicInfo[], el) => {
                if(el.isSelect)
                    acc.push({
                        label: el.label,
                        id: el.id,
                        price:el.price
                    });
                return acc;
            }, []);
            onChange(products);
        }

        setList(listUpdated);
    };

    return (
        <div className={`dropContainer ${classBtn || ''}`}>
            {/* Button Drop */}
            <div className={`buttonDrop w-100 mt-1`} onClick={ !disabled ? toggleMenu : undefined }>
                {
                    labelItemsSelected.length > 0
                    ? labelItemsSelected
                    : labelBtn
                }
                {!disabled && (
                    <img src={require('../assets/arrow.png')}
                        alt="arrow icon" className={`${menuState && 'up'}`} />
                )}
            </div>

            {/* Menu with items */}
            <div className={`menuItems ${menuState && 'active'}`}>

                {list.map((item: IngredientsType, key: number) => (
                    <p key={key} onClick={() => toggleItemSelect(item)}
                        className={`${item.isSelect ? 'selected' : ''}`}>
                        {item.label} {currency(item.price)}
                    </p>
                ))}

            </div>
        </div>
    )
}

export default Dropdown