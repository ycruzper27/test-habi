
type LoaderType = {
  active: boolean
}

const Loader:React.FC<LoaderType> = ({ active }) => {
  return (
    <>
      {active ? (
        <div className="overlayLoader">
            <div className="lds-facebook">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
      ): null}
    </>
  )
}

export default Loader