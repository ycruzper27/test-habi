import { serverTimestamp } from "firebase/firestore/lite"
import { useState } from "react"
import { connect } from "react-redux"
import { PizzaInfoType, UserAssignPayload } from "../types"
import { currency } from "../utils/pipes"
import Loader from "./Loader"
import * as mapActions from '../store/modules/products/actions'

type AssignBody = {
    assign: UserAssignPayload
}

type TypeProps = {
    product: PizzaInfoType,
    updateProduct: (id:string, { assign }: AssignBody) => void,
    getProducts: () => void,
    closeModal: Function
}

const AssignUser:React.FC<TypeProps> = ({ product, updateProduct, closeModal, getProducts }) => {
    const [user, setUser] = useState({} as UserAssignPayload);
    const [isLoading, setIsLoading] = useState(false);
    const [fieldsIncomplete, setFieldsIncomplete] = useState(false);

    const updateUser = (key: string, payload: any) => {
        setFieldsIncomplete(false);
        setUser({
            ...user,
            [key]: payload
        });
    };

    const assignUser = async () => {
        setIsLoading(true);
        if(user.name && user.phone) {
            // * Dispatch action
            await updateProduct(product.id, { assign: {
                ...user,
                createAt: serverTimestamp()
            } });
            // * after close modal and refresh table
            closeModal();
            await getProducts();
        } else {
            setFieldsIncomplete(true);
        }

        setIsLoading(false);
    }

    return (
        <div className="d-flex w-100 justify-center flex-column align-center pizzaContainer">
            <Loader active={isLoading} />

            <h1 className="text-purple mb-2 mt-2">
                Asignar Pizza | { product.name }
            </h1>

            <p className="text-gray font-sm weight-bold mt-1 mb-1">
                INFORMACIÓN DE LA PIZZA:
            </p>

            <span className="text-gray font-xs weight-bold">
                Nombre: {product.name.toUpperCase()}
            </span>
            <span className="text-gray font-xs weight-bold">Id: {product.id}</span>
            <span className="text-gray font-xs weight-bold mb-2">
                Precio: {currency(product.price)}
            </span>

            { fieldsIncomplete && (
                <p className="text-lightRed weight-bold mb-1 mt-1 font-sm">
                Hay campos que faltan por ingresar información
                </p>
            )}


            <input type="text" placeholder="Nombre" onChange={e => updateUser('name', e.target.value)} className="w-90 mb-2" />
            <input type="number" placeholder="Telefono" onChange={e => updateUser('phone', e.target.value)} className="w-90 mb-2" />

            <button className="w-90 mb-2 purple" onClick={assignUser}>Asignar</button>
        </div>
    )
}

export default connect(null, mapActions)(AssignUser)