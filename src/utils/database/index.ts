import { FirebaseApp, initializeApp } from "firebase/app";
import { Firestore, getFirestore, collection, CollectionReference, DocumentData} from 'firebase/firestore/lite';
import firebaseConfig from './config';

class AppFirebase {

    private _app = {} as FirebaseApp;
    private _db = {} as Firestore;
    private _habiColl = {} as CollectionReference<DocumentData>;

    get fire() {
        return this._app;
    };

    set fire(value) {
        this._app = value;
    };

    get db() {
        return this._db;
    };

    set db(value) {
        this._db = value;
    };

    get habiColl() {
        return this._habiColl;
    };

    set habiColl(value) {
        this._habiColl = value;
    };

    initFirebase() {
        this.fire = initializeApp(firebaseConfig);
        this.db = getFirestore(this.fire);
        this.habiColl = collection(this.db, 'habi');

        console.log('init firebase!', this.habiColl);
    }
}

export default AppFirebase;