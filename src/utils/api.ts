import UtilsApi from "."
import { ApiType } from "../types";

const API = new UtilsApi();

export const fetchData = async (apiInfo: ApiType) => {
    // * In case that doesn't exist the collection
    if(!API.habiColl.firestore) {
        API.initFirebase();
    };

    let response = {};
    switch (apiInfo.method) {
        case 'GET_DOC':
            response = await API.getDocSnap(apiInfo?.payload?.id || '');
            break;
        case 'GET_DOCS':
            response = await API.getDocs();
            break;
        case 'ADD_DOC':
            response = await API.addDoc(apiInfo.payload?.body);
            break;
        case 'UPDATE_DOC':
            response = await API.updateDoc(
                apiInfo.payload?.body,
                apiInfo?.payload?.id || ''
            );
            break;
        default: response = {};
    };
    return response;
}
