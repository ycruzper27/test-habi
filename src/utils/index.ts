
import AppFirebase from './database';
import {
    updateDoc as updateDocF,
    addDoc as addDocF,
    getDocs as getDocsF,
    getDoc as getDocF,
    DocumentData,
    serverTimestamp,
    doc
} from 'firebase/firestore/lite';


class UtilsApi extends AppFirebase {

    async getDocs() {
        try {
            const querySnapshot = await getDocsF(this.habiColl);
            const docs = [] as DocumentData[];
            querySnapshot.forEach(doc => docs.push(doc.data()));
            return docs;
        } catch (err) {
            throw new Error('Ha ocurrido un error trayendo los documentos')
        }
    };

    getDocRef(id: string) {
        try {
            return doc(this.db, 'habi', id);
        } catch (error) {
            throw new Error('Ha ocurrido un error trayendo el documento')
        }
    }

    async getDocSnap(id:string) {
        try {
            const docSnap = await getDocF(await this.getDocRef(id));
            if(docSnap.exists())
                return docSnap.data();
            else  throw new Error(`El documento ${id} no existe en nuestra BD`);

        } catch (error) {
            throw new Error('Ha ocurrido un error trayendo el documento');
        }
    }

    async addDoc(data: any){
        try {
            const docRef = await addDocF(this.habiColl, {
                ...data,
                createAt: serverTimestamp()
            });
            // * After create document, update doc with id;
            const docUpdated = await this.updateDoc({
                id: docRef.id
            }, docRef.id);

            return docUpdated;
        } catch (error) {
            console.log('error');
            throw new Error('Ha ocurrido un error creando el documento')
        }
    };

    async updateDoc(data:any, id: string) {
        try {
            const docRef = await this.getDocRef(id);
            await updateDocF(docRef, {
                ...data,
                updateAt: serverTimestamp()
            });
            return await this.getDocSnap(id);
        } catch (error) {
            throw new Error('Ha ocurrido un error actualizando el documento')
        }
    }
}

export default UtilsApi;
