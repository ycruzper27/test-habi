import { IngredientBasicInfo, IngredientsType } from "../types";

export const currency = (price:number) => {
    return price.toLocaleString('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
    });
};


export const calcTotal = (products: IngredientBasicInfo[]) => {
    return products.reduce((acc, el) => {
        acc += el.price;
        return acc;
    },0);
}

export const labelsIngredients = (
    ingredients:IngredientsType[], allIngredients: boolean = false, allLetters:boolean = false
    ) => {
        let labels = '';
        ingredients.forEach((ingredient) => {
            if(allIngredients) {
                labels += `${ingredient.label}, `
                if(labels.length >= 70 && !allLetters) labels = `${labels.slice(0,70)}...`;

            } else {
                if(ingredient.isSelect)
                    labels += `${ingredient.label}, `
            }
        });

        return labels;
}