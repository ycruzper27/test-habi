import { combineReducers } from "redux";
import { productsReducer } from "./modules/products/reducer";

const reducers = combineReducers({
    productsReducer
});

export default reducers;