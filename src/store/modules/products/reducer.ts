import { ActionType, PizzaInfoType } from "../../../types";
import types from './types';

const INITIAL_STATE = {
    products: [] as PizzaInfoType[],
    product: {} as PizzaInfoType
};

export const productsReducer = (state = INITIAL_STATE, action: ActionType) => {
    switch (action.type) {
        case types.GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case types.GET_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case types.UPDATE_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case types.ADD_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case types.ERROR:
            return state;
        default:
            return state;
    }
}