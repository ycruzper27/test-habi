import { Dispatch } from "redux";
import { PizzaInfoType, UserAssignPayload } from "../../../types";
import { fetchData } from "../../../utils/api";
import types from "./types";

export const dispatchError = (dispatch:Dispatch, error:any) => {
    dispatch({
        type: types.ERROR,
        payload: error
    });
}


export const getProducts = () => async (dispatch: Dispatch) => {
    try {
        const response = await fetchData({
            method: 'GET_DOCS'
        });

        dispatch({
            type:types.GET_PRODUCTS,
            payload:response
        });
    } catch (error) {
        dispatchError(dispatch, error);
    }
};

export const getProduct = (id:string) => async (dispatch: Dispatch) => {
    try {
        const response =  await fetchData({
            method: 'GET_DOC',
            payload: { id }
        });

        dispatch({
            type:types.GET_PRODUCT,
            payload:response
        });
    } catch (error) {
        dispatchError(dispatch, error);
    }
}

export const addProduct = (body: PizzaInfoType) => async (dispatch: Dispatch) => {
    try {

        const response = await fetchData({
            method: 'ADD_DOC',
            payload: { body }
        });
        dispatch({
            type:types.ADD_PRODUCT,
            payload:response
        });

    } catch (error) {
        dispatchError(dispatch, error);
    }
};

export const updateProduct = (id: string, body:any) => async (dispatch:Dispatch) => {
    try {
        const response = await fetchData({
            method: 'UPDATE_DOC',
            payload: { id, body }
        });
        dispatch({
            type: types.UPDATE_PRODUCT,
            payload:response
        });
    } catch (error) {
        dispatchError(dispatch, error);
    }
}

export const setLocalProduct = (product: PizzaInfoType) => async (dispatch:Dispatch) => {
    try {
        dispatch({
            type: types.GET_PRODUCT,
            payload: product
        })
    } catch (error) {
        dispatchError(dispatch, error);
    }
}