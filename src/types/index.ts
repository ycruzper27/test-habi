import { AnyAction, Dispatch } from "redux";
import { addProduct, getProduct } from "../store/modules/products/actions";
import reducers from "../store/reducers";

export type StateAppType = ReturnType<typeof reducers>;

export type ModalType = {
    active: boolean,
    closeModal: Function
}

export type IngredientsArrayType = {
    id: number,
    name: string,
    label: string,
    price: number,
    isSelect: boolean
}

export type ActionType = {
  type: string,
  payload: any
}

export type PizzaPayload = {
  name: string,
  dough: 10000,
  chicken?:5000
  peperoni?:4500
  cheese?:3000
  ham?:3000
  tomato?:3000
  sausages?:3000
  oregano?:3000
  pepper?:3000
  garlic?:3000
  paprika?:3000
  bocadillo?:3000
  nachos?:3000
  pineapple?:3000
  eggs?:3000
  cilantro?:3000
}

export type UserAssignPayload = {
  name: string,
  phone: number,
  createAt: any
}

export type PayloadApiType = {
  id ?: string,
  body ?: PizzaInfoType | UserAssignPayload
  assignOnFather ?: boolean
}

export type ApiType = {
  method: 'GET_DOCS' | 'GET_DOC' | 'UPDATE_DOC' | 'ADD_DOC',
  payload?: PayloadApiType
}

export type IngredientsType = {
  id: number,
  name: string,
  label: string,
  price: number,
  isSelect: boolean
}

export type TypeDropdown = {
  classBtn ?: string,
  items: IngredientsType[],
  labelBtn: string,
  disabled ?: boolean,
  onChange ?: Function
}

export type PizzaInfoType = {
  id: string,
  createAt:any,
  name: string,
  ingredients:IngredientsType[],
  price: number,
  dough: 10000,
  assign: UserAssignPayload
}

export type TypePizzaComponent = {
  mode: 'create' | 'read',
  ingredients: IngredientsType[],
  productSelected: PizzaInfoType,
  getProduct: (id:string) => void,
  getProducts: () => void,
  addProduct: (body:PizzaInfoType) => void,
  closeModal: () => void
}

export type IngredientBasicInfo = {
  label: string,
  price: number,
  id: string | number
}